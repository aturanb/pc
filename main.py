import sys

#Relax edges (Note that comparison sign in condition changed to
#smaller then since we are trying to find longest path)
def relax(adj, distance, u):
    for i in adj[u]:
        if(distance[i[0]] < distance[u] + i[1]):
            distance[i[0]] = distance[u] + i[1]

#Traverse all the adjicents that has not traveled
def simple_dfs(vertex, adj, color, topsorted):
    color[vertex] = "BLACK"
    for v in adj[vertex]:
        if color[v[0]] == "WHITE":
            topsort(v[0], adj, color, topsorted)

#Use simple DFS, as each vertex is finished, inset onto topsorted
def topsort(vertex, adj, color, topsorted):
    simple_dfs(vertex, adj, color, topsorted)
    topsorted.append(vertex)
    
def main():
    
    #--------Parse lines from stdin
    numNodes, numEdges = map(int,sys.stdin.readline().split())
    adj = [[] for i in range(numNodes)]    

    for line in sys.stdin:
        source, dest, weight = map(int,line.rstrip().split())       
        adj[source - 1].append([dest - 1, weight])
    
    #--------topologically sort from 1st vertex (which is in index 0)
    topsorted = []
    distance = [float('-inf') for i in range(numNodes)] #Set the u.d of every vertex to -inf
    color = ["WHITE" for i in range(numNodes)]  #Set every vertex to WHITE (untraveled)

    topsort(0, adj, color, topsorted)
        
    #--------Relax Edges    
    distance[0] = 0 #Set distance of starting vertex to 0
    longest = float('-inf')
    ctr = 0
    while(len(topsorted)> 0):
        vertex = topsorted.pop() 
        relax(adj, distance, vertex)
    
    #--------Compare the distances and get the longest one   
    for i in range(numNodes):
        if(distance[i] == longest and longest != float('-inf')):    
            pass
        elif(distance[i] > longest):
            longest = distance[i]
            
    print(f"Longest Path: {longest}")
        
    
    #--------Parse lines from stdin
if __name__ == '__main__':
    main()

